Jar is inside the "target" folder.

Permissions:
/tppos - bessentials.teleportposition

/gamemode - bessentials.gamemode & bessentials.gamemode.other

/creative - bessentials.creative

/survival - bessentials.survival

/whois - bessentials.whois

/tphere - bessentials.teleporthere

/tp - bessentials.teleport

/tpall - bessentials.teleportall

/staffchat - bessentials.staff

/request - bessentials.staff

/report - bessentials.staff

/rawbc - bessentials.rawbroadcast

/rank - bessentials.rank

/mutechat - bessentials.mutechat

/heal - bessentials.heal & bessentials.heal.other

/god - bessentials.god & bessentials.god.other

/fly - bessentials.fly & bessentials.fly.other

/feed - bessentials.feed & bessentials.feed.other

/clear - bessentials.clear & bessentials.clear.other

/bc - bessentials.broadcast

/bessentials reload - bessentials.reload



Full Server Bypass - bessentials.bypass.slots

Whitelist Bypass - bessentials.bypass.whitelist