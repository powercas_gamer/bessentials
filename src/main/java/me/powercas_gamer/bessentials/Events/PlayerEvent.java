package me.powercas_gamer.bessentials.Events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class PlayerEvent implements Listener {

    @EventHandler
    public void onPlayerLoginEvent(PlayerLoginEvent event) {
        if (event.getResult() == PlayerLoginEvent.Result.KICK_WHITELIST) {
            event.setKickMessage(style("&f&m----------------------------------------------------\n\n&cThe network is currently whitelited.\n&cAdditional info may be found at example.net/discord.\n\n&f&m----------------------------------------------------"));
            if (event.getPlayer().hasPermission("bessentials.bypass.whitelist")) {
                event.allow();
            }
        } else {
            if (event.getResult() == PlayerLoginEvent.Result.KICK_FULL) {
                event.setKickMessage(style("&f&m----------------------------------------------------\n\n&cThe network is currently full.\n&cYou can bypass this by buying a rank at store.example.net.\n\n&f&m----------------------------------------------------"));
                if (event.getPlayer().hasPermission("bessentials.bypass.slots")) {
                    event.allow();
                } else {
                    if (event.getResult() == PlayerLoginEvent.Result.KICK_OTHER) {
                        event.setKickMessage(style("&f&m----------------------------------------------------\n\n&cYou have been kicked, we're unable to find out what caused it. We're sorry for the inconviencence.\n\n&f&m----------------------------------------------------"));
                    }
                }
            }
        }
    }
}
