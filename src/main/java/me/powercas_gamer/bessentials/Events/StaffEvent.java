package me.powercas_gamer.bessentials.Events;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import static me.powercas_gamer.bessentials.Utils.Style.style;
import static me.powercas_gamer.bessentials.Utils.bEssConfig.getConfig;

public class StaffEvent implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (event.getPlayer().hasPermission("bessentials.staff")) {
            Player p = event.getPlayer();
            if (p.hasPermission("bessentials.staff"))
                event.setJoinMessage(style("&7[&bStaff&7] &f" + p.getName() + " &bhas joined &f" + getConfig().getString("Settings.Server_Name") + "&b."));
        }
        return;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (event.getPlayer().hasPermission("bessentials.staff")) {
            Player p = event.getPlayer();
            if (p.hasPermission("bessentials.staff"))
                event.setQuitMessage(style("&7[&bStaff&7] &f" + p.getName() + " &bhas left &f" + getConfig().getString("Settings.Server_Name") + "&b."));
        }
        return;
    }

    @EventHandler
    public void onPlayerjoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if(player.getGameMode() == GameMode.CREATIVE) {
            player.setGameMode(GameMode.SURVIVAL);
            player.sendMessage(style("&eYour gamemode has been set to &asurvival &eto prevent abuse."));
            return;
        }
    }
}

