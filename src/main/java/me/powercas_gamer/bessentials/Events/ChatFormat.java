package me.powercas_gamer.bessentials.Events;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.caching.MetaData;
import me.lucko.luckperms.api.context.ContextManager;
import me.lucko.luckperms.api.manager.GroupManager;
import me.lucko.luckperms.api.manager.UserManager;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class ChatFormat implements Listener {

    /**
     * To Be Added: Add Vault Support (Prefixes and Suffixes)
     */

    private String format;
    //private String message;
    //private String prefix;
    //private String suffix;
    private Chat vault = null;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        Player player = e.getPlayer();

        LuckPermsApi api = LuckPerms.getApi();
        User user = api.getUserManager().getUser(player.getUniqueId());
        ContextManager cm = api.getContextManager();
        UserManager userManager = api.getUserManager();
        GroupManager groupManager = api.getGroupManager();
        Contexts contexts = cm.lookupApplicableContexts(user).orElse(cm.getStaticContexts());
        MetaData metaData = user.getCachedData().getMetaData(contexts);
        String prefix = metaData.getPrefix();
        String suffix = metaData.getSuffix();
        String message = e.getMessage();
        String displayname = player.getDisplayName();


        String format = style(prefix + displayname /*+ suffix*/ + ChatColor.GRAY + ": " + ChatColor.WHITE + message);
        e.setFormat(format);
    }
}