package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class ClearCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("bessentials.clear")) {
                sender.sendMessage(style("&cNo Permissions"));
            } else {
                if (args.length < 1) {
                    sender.sendMessage(style("&eClearing your inventory."));
                    player.getInventory().clear();
                    player.getInventory().setBoots(new ItemStack(Material.AIR));
                    player.getInventory().setLeggings(new ItemStack(Material.AIR));
                    player.getInventory().setChestplate(new ItemStack(Material.AIR));
                    player.getInventory().setHelmet(new ItemStack(Material.AIR));
                } else {
                    if (!player.hasPermission("bessentials.clear.other")) {
                        sender.sendMessage(style("&cNo Permissions"));
                    } else {
                        if (args.length < 1) {
                            sender.sendMessage(style("&cUsage: &7/" + 1 + "&b<target>"));
                        } else {
                            Player target = Bukkit.getPlayerExact(args[0]);
                            if (target == null) {
                                sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                            } else {
                                sender.sendMessage(style("&eClearing &c " + target.getName() + " &etheir inventory."));
                                target.getInventory().clear();
                                target.getInventory().setBoots(new ItemStack(Material.AIR));
                                target.getInventory().setLeggings(new ItemStack(Material.AIR));
                                target.getInventory().setChestplate(new ItemStack(Material.AIR));
                                target.getInventory().setHelmet(new ItemStack(Material.AIR));
                                for (Player all : Bukkit.getOnlinePlayers()) {
                                    if (all.hasPermission("bessentials.clear.other")) {
                                        if (all.getName() != player.getName()) {
                                            all.sendMessage(style("&7&o[" + player.getName() + ": &7cleared " + target.getName() + "'s inventory&7&o]"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}