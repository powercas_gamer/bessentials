package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class CopyInvCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("bessentials.clear")) {
                sender.sendMessage(style("&cNo Permissions"));
            } else {
                if (args.length < 1) {
                    sender.sendMessage(style("&cUsage: &7/" + alias + " &b<player>"));
                } else {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target == null) {
                        sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                    } else {
                        sender.sendMessage(style("&eYou have coppied &c" + target.getName() + "'s &einventory."));
                        player.getInventory().setArmorContents(target.getInventory().getArmorContents());
                        player.getInventory().setContents(target.getInventory().getContents());
                    }
                }
            }
        }
        return true;
    }
}