package me.powercas_gamer.bessentials.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class MuteChatCMD implements CommandExecutor {

    /*
        Broken but w/e
     */

    private volatile boolean chatEnabled = true;

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (!event.getPlayer().hasPermission("bessentials.mutechat.bypass")) {
        }
        if (!chatEnabled) {
            event.setCancelled(false);
            event.getPlayer().sendMessage(style("&eChat is currently muted!"));
        } else {
            if (chatEnabled) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(style("&eChat is currently muted!"));
            }
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if ((sender instanceof Player) || (sender instanceof ConsoleCommandSender)) {
            Player player = (Player) sender;
            if (!player.hasPermission("bessentials.mutechat")) {
                sender.sendMessage(style("&cNo Permission"));
            } else {
                chatEnabled = !chatEnabled;
                sender.sendMessage(style(chatEnabled ? "&eYou have &cmuted &ethe chat!" : "&eYou have &cunmuted &ethe chat!"));
            }
        }
        return true;
    }
}
