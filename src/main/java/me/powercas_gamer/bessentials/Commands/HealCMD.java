package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class HealCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("bessentials.heal")) {
                sender.sendMessage(style("&cNo Permissions"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&eYou've been healed!"));
                player.setFoodLevel(20);
                player.setHealth(20);
            } else {
                if (!player.hasPermission("bessentials.heal.other")) {
                    sender.sendMessage(style("&cNo Permission"));
                }
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target == null) {
                    sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                } else {
                    if (args.length < 1) {
                        sender.sendMessage(style("&cUsage: &7/" + label + " &b<player>"));
                    } else {
                        sender.sendMessage(style("&eYou have healed &a" + target.getName() + " &e."));
                        target.sendMessage(style("&eYou've been healed!"));
                        target.setFoodLevel(20);
                        target.setHealth(20);
                    }
                }
            }
        }
        return true;
    }
}