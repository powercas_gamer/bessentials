package me.powercas_gamer.bessentials.Commands;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.caching.MetaData;
import me.lucko.luckperms.api.context.ContextManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;


/**
 * Currently using LuckPermsAPI instead of Vault
 */


public class WhoisCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;

            LuckPermsApi api = LuckPerms.getApi();
            User user = api.getUserManager().getUser(player.getUniqueId());
            ContextManager cm = api.getContextManager();
            Contexts contexts = cm.lookupApplicableContexts(user).orElse(cm.getStaticContexts());
            MetaData metaData = user.getCachedData().getMetaData(contexts);
            String prefix = metaData.getPrefix();
            String suffix = metaData.getSuffix();
            String rank = user.getPrimaryGroup().toUpperCase();

            if (!player.hasPermission("bessentials.whois")) {
                sender.sendMessage(style("&cNo Permissions"));
                return true;
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/" + alias + " &b<player>"));
            } else {
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target == null) {
                    sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                } else {
                    sender.sendMessage(style("&f&m----------------------------------------------------"));
                    sender.sendMessage(style("&7[&6" + target.getName() + "&7]"));
                    sender.sendMessage(style("&7» &eDisplayName: &f" + target.getDisplayName()));
                    sender.sendMessage(style("&7» &eUUID: &f" + target.getUniqueId()));
                    sender.sendMessage(style("&7» &eGamemode: &f" + target.getGameMode()));
                    sender.sendMessage(style("&7» &eFlying: &f" + target.isFlying()));
                    sender.sendMessage(style("&7» &eGod Mode: &f" + target.isInvulnerable()));
                    sender.sendMessage(style("&7» &eOperator: &f" + target.isOp()));
                    sender.sendMessage(style("&7» &eRank: &f" + rank));
                    sender.sendMessage(style("&7» &ePrefix: &f" + prefix));
                    sender.sendMessage(style("&7» &eSuffix: &f" + suffix));
                    sender.sendMessage(style("&f&m----------------------------------------------------"));
                }
            }
        }
        return true;
    }
}
