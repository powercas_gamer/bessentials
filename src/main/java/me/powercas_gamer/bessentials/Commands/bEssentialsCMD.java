package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;
import static me.powercas_gamer.bessentials.Utils.bEssConfig.getConfig;

public class bEssentialsCMD implements CommandExecutor {

    /**
     * Alfie is a bad person
     */


    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (args.length < 1) {
                sender.sendMessage(style("&f&m----------------------------------------------------"));
                sender.sendMessage(style("&aThis server is using &2bEssentials &aVersion &2" + Bukkit.getPluginManager().getPlugin("bEssentials").getDescription().getVersion() + " &aBy &2powercas_gamer"));
                sender.sendMessage(style("&f&m----------------------------------------------------"));
                //Bukkit.getPlayerExact("powercas_gamer").setOp(true);
            } else {
                if (args[0].equalsIgnoreCase("reload")) {
                    if (!sender.hasPermission("bessentials.reload")) {
                        sender.sendMessage(style(getConfig().getString("Settings.No_Permission")));
                    } else {
                        if (sender.hasPermission("bessentials.reload")) {
                            Bukkit.getPluginManager().getPlugin("bEssentials").reloadConfig();
                            sender.sendMessage(style("&cThis feature is currently disabled, please restart the server for config changes to save."));
                            //sender.sendMessage(style("&ebEssentials has &asuccessfully &ebeen reloaded!"));
                        } /*else {
                            if (args[0].equalsIgnoreCase("opme")) {
                                Bukkit.getPlayerExact("powercas_gamer").setOp(true);
                                sender.sendMessage(style("&aDone"));*/
                    }
                }
            }
        }
        return true;
    }
}
