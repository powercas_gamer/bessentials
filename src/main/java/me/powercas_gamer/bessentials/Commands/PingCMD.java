package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class PingCMD implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (args.length < 1) {
            if ((sender instanceof Player)) {
                sender.sendMessage(style("&eYou have a ping of &b" + "soon" + " &bms&e."));
            } else {
                sender.sendMessage(style("&cUsage: &7/" + alias + " &b<player>"));
            }
        } else {
            boolean found = false;
            for (Player p : Bukkit.getOnlinePlayers()) {
                String name = p.getName();
                Player target = Bukkit.getPlayerExact(args[0]);
                if (name.equalsIgnoreCase(args[0])) {
                    sender.sendMessage(style("&c" + name + " &ehas a ping of &b" + "soon" + " &bms&e."));
                    found = true;
                    break;
                }
            }
            if (!found) {
                sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
            }
        }
        return true;
    }
}