package me.powercas_gamer.bessentials.Commands;

import com.sun.scenario.Settings;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;
import static me.powercas_gamer.bessentials.Utils.bEssConfig.getConfig;

public class StaffChatCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        Player player = (Player) sender;
        if (!player.hasPermission("bessentials.staff")) {
            sender.sendMessage("&cNo Permissions");
        }
        if (args.length < 1) {
            sender.sendMessage(style("&cUsage: &7/" + alias + " &b<message...>"));
        } else {
            String b = StringUtils.join(args, " ");
            if (player.hasPermission("bessentials.staff"))
                Bukkit.broadcastMessage(style(getConfig().getString("Messages.StaffChat")));//.replace("%player%", player.getName().replace("%server%", getConfig().getString("Settings.ServerName"))));
            //getConfig().getString("Messages.StaffChat").replace("%player%, player.getName())
            //b.replace('server%', 'getConfig().getString("Settings.Server_Name")');
            //b.replace("%message%", b);
        }
        return true;
    }
}