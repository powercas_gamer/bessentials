package me.powercas_gamer.bessentials.Commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static me.powercas_gamer.bessentials.Utils.Style.style;
import static me.powercas_gamer.bessentials.Utils.bEssConfig.getConfig;

public class BroadcastCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("bessentials.broadcast")) {
            sender.sendMessage(style("&cNo Permissions"));
        } else {
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/" + alias + " &b<message...>"));
            } else {
                String b = StringUtils.join(args, " ");
                for (String msg : getConfig().getStringList("Messages.Broadcast")) {
                    b = b.replace("%message%", StringUtils.join(args, " "));
                    sender.sendMessage(style(msg));
                }
            }
        }
        return true;
    }
}
