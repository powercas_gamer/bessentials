package me.powercas_gamer.bessentials.Commands;

import org.bukkit.command.CommandExecutor;

import me.powercas_gamer.bessentials.Managers.CooldownManager;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import static me.powercas_gamer.bessentials.Utils.Style.style;
import static me.powercas_gamer.bessentials.Utils.bEssConfig.getConfig;

public class ReportCMD implements CommandExecutor {

    private final CooldownManager cooldownManager = new CooldownManager();

    private final Plugin plugin;

    public ReportCMD(Plugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //Player only command
        if (sender instanceof Player) {
            Player player = (Player) sender;
            int timeLeft = cooldownManager.getCooldown(player.getUniqueId());
            //If the cooldown has expired
            if (args.length < 2) {
                sender.sendMessage(style("&cUsage: &7/" + label + " &b<playerName> <message...>"));
            } else {
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target == null) {
                    sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                } else {
                    if (timeLeft == 0) {
                        //Use the command
                        String b = StringUtils.join(args, " ");
                        if (player.hasPermission("bessentials.staff"))
                            Bukkit.broadcastMessage(style(getConfig().getString("Messages.Report")));
                        b = b.replace("%player%", sender.getName());
                        b = b.replace("%target%", target.getName());
                        b = b.replace("%server%", getConfig().getString("Settings.Server_Name"));
                        b = b.replace("%server_target%", getConfig().getString("Settings.Server_Name"));
                        b = b.replace("%reason%", b);
                        sender.sendMessage(style(getConfig().getString("Responses.Report")));
                        //Start the countdown task
                        cooldownManager.setCooldown(player.getUniqueId(), CooldownManager.DEFAULT_COOLDOWN);
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                int timeLeft = cooldownManager.getCooldown(player.getUniqueId());
                                cooldownManager.setCooldown(player.getUniqueId(), --timeLeft);
                                if (timeLeft == 0) {
                                    this.cancel();
                                }
                            }
                        }.runTaskTimer(this.plugin, 20, 20);

                    } else {
                        //Hasn't expired yet, shows how many seconds left until it does
                        player.sendMessage(style("&eYou have to wait &a" + timeLeft + " &eseconds before using this command again!"));
                    }
                }
            }
        }
        return true;
    }
}