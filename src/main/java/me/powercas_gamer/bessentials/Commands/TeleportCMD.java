package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class TeleportCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("bessentials.teleport")) {
                sender.sendMessage(style("&cNo Permission"));
                return true;
            } else if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/" + alias + " &b<player>"));
                return true;
            } else {
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target == null) {
                    sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                } else {
                    sender.sendMessage(style("&eTeleporting to &c" + target.getName() + "&e."));
                    player.teleport(target);
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        if (all.hasPermission("bessentials.teleport")) {
                            if (all.getName() != player.getName()) {
                                all.sendMessage(style("&7&o[" + player.getName() + ": &7teleported " + player.getName() + " to " + target.getName() + "&7&o]"));
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
