package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class TeleportALLCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("bessentials.teleportall")) {
                sender.sendMessage(style("&cNo Permission"));
                return true;
            } else {
                if (args.length < 1) {
                    sender.sendMessage(style("&cUsage: &7/" + alias));
                } else {
                    for (Player o : Bukkit.getOnlinePlayers()) {
                        if (o.getPlayer() != player) {
                            if (o == null) {
                                sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                            } else {
                                sender.sendMessage(style("&eTeleporting &c" + Bukkit.getServer().getOnlinePlayers() + " player(s) &eto yourself."));
                                o.sendMessage(style("&eYou've been teleported to &c" + sender.getName() + "&e."));
                                o.teleport(player);
                                for (Player all : Bukkit.getOnlinePlayers()) {
                                    if (all.hasPermission("bessentials.teleportall")) {
                                        if (all.getName() != player.getName()) {
                                            all.sendMessage(style("&7&o[" + player.getName() + ": &7teleported " + o + " to " + player.getName() + "&7&o]"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}