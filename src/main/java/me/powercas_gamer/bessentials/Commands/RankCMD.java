package me.powercas_gamer.bessentials.Commands;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.context.ContextManager;
import me.lucko.luckperms.api.manager.GroupManager;
import me.lucko.luckperms.api.manager.UserManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class RankCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        Player player = (Player) sender;
        Player target = Bukkit.getPlayerExact(args[0]);

        LuckPermsApi api = LuckPerms.getApi();
        User user = api.getUserManager().getUser(target.getUniqueId());
        ContextManager cm = api.getContextManager();
        UserManager userManager = api.getUserManager();
        GroupManager groupManager = api.getGroupManager();

        if (!sender.hasPermission("bessentials.rank")) {
            sender.sendMessage(style("&cNo Permissions"));
        }
        if (args.length < 1) {
            sender.sendMessage(style("no thing add l8er"));
        } else {
            if (target == null) {
                sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
            } else {
                String rank = args[1];
                user.setPrimaryGroup(args[1]);
                sender.sendMessage(style("&eYou have set &c" + target.getName() + " &erank to &c" + rank + "&e."));
                target.sendMessage(style("&eYour rank has been set to &c" + rank + "&e."));
            }
        }
        return true;
    }
}