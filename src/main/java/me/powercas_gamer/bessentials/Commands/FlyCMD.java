package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class FlyCMD implements CommandExecutor {

    private List<Player> fly = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player player = (Player) sender;
        if (!player.hasPermission("bessentials.fly")) {
            sender.sendMessage(style("&cNo Permission."));
        } else {
            if (!(fly.contains(player))) {
                player.sendMessage(style("&eYou have &aenabled &eyour fly mode"));
                player.setAllowFlight(true);
                fly.add(player);
            } else {
                if (fly.contains(player)) {
                    player.sendMessage(style("&eYou have &cdisabled &eyour fly mode."));
                    player.setAllowFlight(false);
                    fly.remove(player);
                } else {
                    if (!player.hasPermission("bessentials.fly.other")) {
                        sender.sendMessage(style("&cNo Permissions"));
                    } else {
                        Player target = Bukkit.getPlayerExact(args[0]);
                        if (target == null) {
                            sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                        } else {
                            if (args.length < 1) {
                                sender.sendMessage(style("&cUsage: &7/" + label + " &b<target>"));
                            } else {
                                if (!(fly.contains(target))) {
                                    sender.sendMessage(style("&eYou have &aenabled &9" + target.getName() + "&etheir fly mode."));
                                    target.sendMessage(style("&eYour fly mode has been &aenabled&e."));
                                    target.setAllowFlight(true);
                                    fly.add(target);
                                } else if (fly.contains(target)) {
                                    sender.sendMessage(style("&eYou have &cdisabled &9" + target.getName() + "&etheir fly mode."));
                                    target.sendMessage(style("*eYour fly mode has been &cdisabled&e."));
                                    target.setAllowFlight(false);
                                    fly.remove(target);
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}