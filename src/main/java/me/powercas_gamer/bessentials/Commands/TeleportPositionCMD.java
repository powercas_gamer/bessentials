package me.powercas_gamer.bessentials.Commands;

import com.google.common.primitives.Ints;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class TeleportPositionCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("bessentials.teleportposition")) {
                sender.sendMessage(style("&cNo Permission"));
                return true;
            }
            if (args.length < 3) {
                //player.sendMessage(style("&cUsage: &7/" + alias + "&b<x> <y> <z>"));
                player.sendMessage(style("&cThis command is currently disabled."));
                return true;
            }
        }
        return true;
    }
}