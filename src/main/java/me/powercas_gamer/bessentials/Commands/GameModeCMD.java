package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class GameModeCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        Player player = (Player) sender;
        if (!player.hasPermission("bessentials.gamemode")) {
            sender.sendMessage(style("&cNo Permission"));
        } else {
            if (args.length == 0) {
                sender.sendMessage(style("&cUsage: &7/" + alias + " &b<player> <survival|creative>"));
            } else {
                if (args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("s")) {
                    sender.sendMessage(style("&eYour gamemode has been set to &cSurvival&e."));
                    player.setGameMode(GameMode.SURVIVAL);
                } else {
                    if (args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("c")) {
                        sender.sendMessage(style("&eYour gamemode has been set to &cCreative&e."));
                        player.setGameMode(GameMode.CREATIVE);
                    } else {
                        if (!player.hasPermission("bessentials.gamemode.other")) {
                            sender.sendMessage(style("&cNo Permission"));
                        }
                            Player target = Bukkit.getPlayerExact(args[0]);
                            if (target == null) {
                                sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                            } else {
                                if (args.length < 1) {
                                    sender.sendMessage(style("&cUsage: &7/" + alias + " &b<player> <survival|creative>"));
                                } else {
                                    if (args[1].equalsIgnoreCase("survival") || args[1].equalsIgnoreCase("0") || args[1].equalsIgnoreCase("s")) {
                                        sender.sendMessage(style("&eYou have set &c" + target.getName() + " &egamemode to &cSurvival&e."));
                                        target.sendMessage(style("&eYour gamemode has been set to &cSurvival&e."));
                                        target.setGameMode(GameMode.SURVIVAL);
                                    } else {
                                        if (args[1].equalsIgnoreCase("creative") || args[1].equalsIgnoreCase("1") || args[1].equalsIgnoreCase("c")) {
                                            sender.sendMessage(style("&eYou have set &c" + target.getName() + " &egamemode to &cCreative&e."));
                                            target.sendMessage(style("&eYour gamemode has been set to &cCreative&e."));
                                            target.setGameMode(GameMode.CREATIVE);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        return true;
    }
}
