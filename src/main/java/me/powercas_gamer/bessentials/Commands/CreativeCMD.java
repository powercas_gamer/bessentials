package me.powercas_gamer.bessentials.Commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class CreativeCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("bessentials.creative")) {
                sender.sendMessage(style("&cNo Permissions."));
            } else {
                sender.sendMessage(style("&eYour gamemode has been set to &cCreative&e."));
                player.setGameMode(GameMode.CREATIVE);
            }
        }
        return true;
    }
}
