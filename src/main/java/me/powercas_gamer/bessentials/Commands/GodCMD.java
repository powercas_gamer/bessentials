package me.powercas_gamer.bessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public class GodCMD implements CommandExecutor {

    private List<Player> god = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player player = (Player) sender;
        if (!player.hasPermission("bessentials.god")) {
            sender.sendMessage(style("&cNo Permission."));
        } else {
            if (!(god.contains(player))) {
                player.sendMessage(style("&eYou have &aenabled &eyour god mode"));
                player.setInvulnerable(true);
                god.add(player);
            } else {
                if (god.contains(player)) {
                    player.sendMessage(style("&eYou have &cdisabled &eyour god mode."));
                    player.setInvulnerable(false);
                    god.remove(player);
                } else {
                    if (!player.hasPermission("bessentials.god.other")) {
                        sender.sendMessage(style("&cNo Permissions"));
                    } else {
                        Player target = Bukkit.getPlayerExact(args[0]);
                        if (target == null) {
                            sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                        } else {
                            if (args.length < 1) {
                                sender.sendMessage(style("&cUsage: &7/" + label + " &b<target>"));
                            } else {
                                if (!(god.contains(target))) {
                                    sender.sendMessage(style("&eYou have &aenabled &9" + target.getName() + "&etheir god mode."));
                                    target.sendMessage(style("&eYour god mode has been &aenabled&e."));
                                    target.setInvulnerable(true);
                                    god.add(target);
                                } else if (god.contains(target)) {
                                    sender.sendMessage(style("&eYou have &cdisabled &9" + target.getName() + "&etheir god mode."));
                                    target.sendMessage(style("&eYour god mode has been &cdisabled&e."));
                                    target.setInvulnerable(false);
                                    god.remove(target);
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}