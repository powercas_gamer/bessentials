package me.powercas_gamer.bessentials.Commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static me.powercas_gamer.bessentials.Utils.Style.style;
import static me.powercas_gamer.bessentials.Utils.bEssConfig.getConfig;

public class RawBroadcastCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("bessentials.rawbroadcast")) {
            sender.sendMessage(style("&cNo Permissions"));
        } else {
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/" + alias + " &b<message...>"));
            } else {
                for (String msg : getConfig().getStringList("Messages.RawBroadcast")) {
                    sender.sendMessage(style(msg));
                    msg = msg.replace("%message%", msg);
                }
            }
        }
        return true;
    }
}
//                 String msg = StringUtils.join(args, " ");