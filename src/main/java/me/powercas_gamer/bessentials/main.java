package me.powercas_gamer.bessentials;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.LuckPermsApi;
import me.powercas_gamer.bessentials.Commands.*;
import me.powercas_gamer.bessentials.Events.ChatFormat;
import me.powercas_gamer.bessentials.Events.PlayerEvent;
import me.powercas_gamer.bessentials.Events.StaffEvent;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import static me.powercas_gamer.bessentials.Utils.Style.style;

public final class main extends JavaPlugin {


    private static Chat chat = null;

    @Override
    public void onEnable() {
        System.out.println("[bEssentials] Registering Commands...");
        this.registerCommands();
        System.out.println("[bEssentials] Successfully registered commands!");
        System.out.println("[bEssentials] Registering Events...");
        this.registerEvents();
        System.out.println("[bEssentials] Successfully registered events!");
        System.out.println("[bEssentials] Registering Managers...");
        this.registerManagers();
        System.out.println("[bEssentials] Successfully registered managers!");
        this.registerConfig();
        System.out.println("[bEssentials] Plugin Loaded!");
        LuckPermsApi api = LuckPerms.getApi();
    }


    @Override
    public void onDisable() {
        System.out.println("[bEssentials] Disabling Plugin...");
        System.out.println("[bEssentials] Kicking players..");
        this.kickPlayers();
        System.out.println("[bEssentials] Successfully kicked players!");
    }


    public void registerCommands() {
        this.getCommand("bessentials").setExecutor(new bEssentialsCMD());
        this.getCommand("broadcast").setExecutor(new BroadcastCMD());
        this.getCommand("rawbroadcast").setExecutor(new RawBroadcastCMD());
        this.getCommand("teleport").setExecutor(new TeleportCMD());
        this.getCommand("teleporthere").setExecutor(new TeleportHereCMD());
        this.getCommand("creative").setExecutor(new CreativeCMD());
        this.getCommand("survival").setExecutor(new SurvivalCMD());
        this.getCommand("fly").setExecutor(new FlyCMD());
        this.getCommand("clear").setExecutor(new ClearCMD());
        this.getCommand("gamemode").setExecutor(new GameModeCMD());
        this.getCommand("teleportposition").setExecutor(new TeleportPositionCMD());
        this.getCommand("mutechat").setExecutor(new MuteChatCMD());
        this.getCommand("whois").setExecutor(new WhoisCMD());
        this.getCommand("staffchat").setExecutor(new StaffChatCMD());
        this.getCommand("request").setExecutor(new RequestCMD(this));
        this.getCommand("report").setExecutor(new ReportCMD(this));
        this.getCommand("heal").setExecutor(new HealCMD());
        this.getCommand("teleportall").setExecutor(new TeleportALLCMD());
        this.getCommand("copyinv").setExecutor(new CopyInvCMD());
        this.getCommand("feed").setExecutor(new FeedCMD());
        this.getCommand("ping").setExecutor(new PingCMD());
        this.getCommand("god").setExecutor(new GodCMD());
        this.getCommand("rank").setExecutor(new RankCMD());
    }

    public void registerEvents() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(new ChatFormat(), this);
        pm.registerEvents(new StaffEvent(), this);
        pm.registerEvents(new PlayerEvent(), this);
    }

    public void registerManagers() {
        System.out.println("[bEssentials] Registering Report Cooldown!");
        System.out.println("[bEssentials] Successfully registered Report Cooldown!");
        System.out.println("[bEssentials] Registering Request Cooldown!");
        System.out.println("[bEssentials] Successfully registered Request Cooldown!");

    }

    public void kickPlayers() {
        for (Player player : getServer().getOnlinePlayers()) {
            player.kickPlayer(style("&f&m----------------------------------------------------\n\n&4&lThe server is currently rebooting...\n\n&f&m----------------------------------------------------"));
        }

    }

    /*private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }*/

    private boolean setupLP() {
        RegisteredServiceProvider<LuckPermsApi> provider = Bukkit.getServicesManager().getRegistration(LuckPermsApi.class);
        if (provider != null) {
            LuckPermsApi api = provider.getProvider();
        }
        return false;
    }

    private boolean setupVault() {
        if (Bukkit.getPluginManager().getPlugin("Vault") == null) {
        }
        return false;
    }

    public Chat getChat() {
        return chat;
    }

    private void registerConfig() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }
        saveDefaultConfig();
        getConfig();
    }
}
